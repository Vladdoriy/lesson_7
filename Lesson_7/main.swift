//Описать несколько структур – любой легковой автомобиль и любой грузовик. Структуры должны содержать марку авто, год выпуска, объем багажника/кузова, запущен ли двигатель, открыты ли окна, заполненный объем багажника
//▸ Описать перечисление с возможными действиями с автомобилем: запустить/заглушить двигатель, открыть/закрыть окна, погрузить/выгрузить из кузова/багажника груз определенного объема
//▸ Добавить в структуры метод с одним аргументом типа перечисления, который будет менять свойства структуры в зависимости от действия
//▸ Инициализировать несколько экземпляров структур. Применить к ним различные действия. Положить объекты структур в словарь как ключи, а их названия как строки например var dict = [structCar: 'structCar']
enum СarActions {
    case engineStart
    case engineStop
    case windowsOpen
    case windowClosed
    case trunkLoad
    case trunkUnload
}

struct Avto {
    var brandOfCar: String
    var yearRelease: Int
    var trunkVolume: Int
    var engineСondition: Bool
    var windowsCondition: Bool
    var filledTruncVolume: Int?
    
    func actionsWithTheCar (action: СarActions) {
        switch action{
        
        case .engineStart:
            print("Двигатель \(brandOfCar) заведен")
        
        case .engineStop:
            print("Двигатель \(brandOfCar) заглушен")

        case .windowsOpen:
            print("Окна в \(brandOfCar) открыты")

        case .windowClosed:
            print("Окна в \(brandOfCar) закрыты")
        
        case .trunkLoad:
            print("Багажник \(brandOfCar) загружен" )
       
        case .trunkUnload:
            print("Багажник \(brandOfCar) разгружен" )
            
        }
    }
}

struct Truck {
    var brandOfCar: String
    var yearRelease: Int
    var trunkVolume: Int
    var engineСondition: Bool
    var windowsCondition: Bool
    var filledTruncVolume: Int?
    func actionsWithTheCar (action: Truck){
        
    }
}

let car1 = Avto(brandOfCar: "Golf", yearRelease: 2019, trunkVolume: 1000, engineСondition: true, windowsCondition: false)

car1.actionsWithTheCar(action: .engineStart)
car1.actionsWithTheCar(action: .windowClosed)
car1.actionsWithTheCar(action: .trunkLoad)

//Набрать код который на скриншоте понять в чем там проблема и решить эту проблему
class Car {
    weak var driver: Man?
    deinit{
        print ("машина удалена из памяти")
    }
}

class Man {
    var myCar: Car?
    
    deinit {
        print("мужчина удален из памяти")
    }
}
var car: Car? = Car()
var man: Man? = Man()

car?.driver = man
man?.myCar = car
car = nil
man = nil
// в конце мы задаем car = nil и man = nil , мы не можем этого сделать, пока между каром и мэном есть ссылка, как только мы поменяли var на weak var,  все стало отлично
